/**
 ******************************************************************************
 * @file	: gpio_api.h
 * @version	: V0.0.1
 * @date	: 2023.09.02
 * @author	: Tony Liu
 ******************************************************************************
 *
 *
 *
 ******************************************************************************
 */

 /* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _GPIO_API_H_
#define _GPIO_API_H_

/* Predefined Or Compiler Option ---------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
/* Hardware Configuration ----------------------------------------------------*/
/* Define --------------------------------------------------------------------*/
/* Enum ----------------------------------------------------------------------*/
/* Marco ---------------------------------------------------------------------*/
/* Typedef -------------------------------------------------------------------*/
/* Public Variable -----------------------------------------------------------*/
/* Public Class --------------------------------------------------------------*/
/* Public Function -----------------------------------------------------------*/
extern int configure_gpio_as_output(const struct gpio_dt_spec *output_pin);
extern int configure_gpio_as_input(const struct gpio_dt_spec *input_pin);
extern int toggle_gpio(const struct gpio_dt_spec *output_pin);
extern int set_gpio_as_high(const struct gpio_dt_spec *output_pin);
extern int set_gpio_as_low(const struct gpio_dt_spec *output_pin);
extern int read_gpio_status(const struct gpio_dt_spec *input_pin);

#ifdef __cplusplus
}
#endif

#endif

/** 
 ******************************************************************************
 * @file	: gpio_api.c
 * @version	: V0.0.1
 * @date	: 2023.09.02
 * @author	: Tony Liu
 ******************************************************************************
 *
 *
 *
 ******************************************************************************
 */

/* Predefined Or Compiler Option ---------------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include <zephyr/drivers/gpio.h>
#include "gpio_api.h"

/* Private Typedef -----------------------------------------------------------*/
/* Private Prototype ---------------------------------------------------------*/
/* Private Define ------------------------------------------------------------*/
/* Private Enum --------------------------------------------------------------*/
/* Private Marco -------------------------------------------------------------*/
/* Private Variable ----------------------------------------------------------*/

/* Public Variable -----------------------------------------------------------*/
/* Import Variable -----------------------------------------------------------*/
/* Import Function -----------------------------------------------------------*/
/* Private Class -------------------------------------------------------------*/
/* Public Class --------------------------------------------------------------*/
/* Private Function ----------------------------------------------------------*/
/**
  * @brief	
  * @note	
  * @param	
  * @retval	
  * @return	
  */ 

int configure_gpio_as_output(const struct gpio_dt_spec *output_pin)
{
  DEBUG_ASSERT(output_pin != NULL);
  int ret;

  if (!gpio_is_ready_dt(output_pin)) {
      return EXCEPTION;
  }

  ret = gpio_pin_configure_dt(output_pin, GPIO_OUTPUT_ACTIVE);
  if (ret < 0) {
      return ret;
  }

  return SUCCESS;
}

int configure_gpio_as_input(const struct gpio_dt_spec *input_pin)
{
  DEBUG_ASSERT(input_pin != NULL);
  int ret;

  if (!gpio_is_ready_dt(input_pin)) {
      return EXCEPTION;
  }

  ret = gpio_pin_configure_dt(input_pin, GPIO_INPUT);
  if (ret < 0) {
      return ret;
  }

  return SUCCESS;
}

int toggle_gpio(const struct gpio_dt_spec *output_pin)
{
  DEBUG_ASSERT(output_pin != NULL);
  int ret;

  ret = gpio_pin_toggle_dt(output_pin);
  if (ret < 0) {
      return ret;
  }

  return SUCCESS;
}

int set_gpio_as_high(const struct gpio_dt_spec *output_pin)
{
  DEBUG_ASSERT(output_pin != NULL);
  int ret;

  ret = gpio_pin_set_dt(output_pin, HIGH);
  if (ret < 0) {
      return ret;
  }

  return SUCCESS;
}

int set_gpio_as_low(const struct gpio_dt_spec *output_pin)
{
  DEBUG_ASSERT(output_pin != NULL);
  int ret;

  ret = gpio_pin_set_dt(output_pin, LOW);
  if (ret < 0) {
      return ret;
  }

  return SUCCESS;
}
  
int read_gpio_status(const struct gpio_dt_spec *input_pin)
{
  DEBUG_ASSERT(input_pin != NULL);
  return (gpio_pin_get_dt(&input_pin));
}


/* Public Function -----------------------------------------------------------*/
/**
  * @brief	
  * @note	
  * @param	
  * @retval	
  * @return	
  */ 

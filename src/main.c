/** 
 ******************************************************************************
 * @file	: main.c
 * @version	: V0.0.1
 * @date	: 2023.09.
 * @author	: 
 ******************************************************************************
 *
 *
 *
 ******************************************************************************
 */
/*
 * Copyright (c) 2012-2014 Wind River Systems, Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/* Predefined Or Compiler Option ---------------------------------------------*/
#define ENABLE_MAIN_DEBUG_MSG

/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include "live_led.h"

/* Private Typedef -----------------------------------------------------------*/
/* Private Prototype ---------------------------------------------------------*/
/* Private Define ------------------------------------------------------------*/
/* Private Enum --------------------------------------------------------------*/
/* Private Marco -------------------------------------------------------------*/
/* Private Variable ----------------------------------------------------------*/
#if defined(ENABLE_MAIN_DEBUG_MSG) && defined(ENABLE_DEBUG_MSG)
#define debug_message(...) \
STATEMENT(\
  printk(__VA_ARGS__);\
)
#else
#define debug_message(...) {;}
#endif

/* Public Variable -----------------------------------------------------------*/
/* Import Variable -----------------------------------------------------------*/
/* Import Function -----------------------------------------------------------*/
/* Private Class -------------------------------------------------------------*/
/* Public Class --------------------------------------------------------------*/
/* Private Function ----------------------------------------------------------*/
/**
  * @brief	
  * @note	
  * @param	
  * @retval	
  * @return	
  */ 
  
/* Public Function -----------------------------------------------------------*/
/**
  * @brief	
  * @note	
  * @param	
  * @retval	
  * @return	
  */ 


int main(void)
{
  debug_message("Hello World! %s\n", CONFIG_BOARD);
  delay(sec(3)); //delay 3 sec

  init_live_led();

	while (1)
	{
		static tick_t timeout = 0;

		if (tick_is_timeout(timeout))
		{
      /* 週期性印出訊息 */
			debug_message("hahaha\n");
			set_tick(&timeout, sec(1));
		}

    update_and_control_live_led();
	}
	
	return 0;
}

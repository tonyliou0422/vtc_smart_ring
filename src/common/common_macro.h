/**
 ******************************************************************************
 * @file	: common_macro.h
 * @version	: V0.0.1
 * @date	: 2023.09.01
 * @author	: Tony Liu
 ******************************************************************************
 *
 *
 *
 ******************************************************************************
 */

 /* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _COMMON_MACRO_H_
#define _COMMON_MACRO_H_

/* Predefined Or Compiler Option ---------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
/* Hardware Configuration ----------------------------------------------------*/
/* Define --------------------------------------------------------------------*/
#ifndef SHIFT
#define SHIFT(n) (1 << (n))
#endif

#ifndef BIT
#define BIT(b) (1 << (b))
#endif

#ifndef STATEMENT
#define STATEMENT(x)      do { x } while (__LINE__ == -1)
#endif

#ifndef ELEMEMT_NUMBER
#define ELEMEMT_NUMBER(x)	(sizeof(x)/sizeof(x[0]))
#endif

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x)	(sizeof(x)/sizeof(x[0]))
#endif

#ifndef ABS
#define ABS(x,y)	(((x)>(y))?((x)-(y)):((y)-(x)))
#endif

#ifndef LBYTE
#define LBYTE(x)		((x)&0xFF)
#endif

#ifndef HBYTE
#define HBYTE(x)		((x)>>8)
#endif

#ifndef BYTE_TO_WORD
#define BYTE_TO_WORD(l, h) (((unsigned short)h<<8) | l)
#endif

#ifndef BYTE_TO_DWORD
#define BYTE_TO_DWORD(l0, l1,h0, h1) (((unsigned long)h1<<24) | ((unsigned long)h0<<16) | ((unsigned long)l1<<8) |(l0))
#endif

#ifndef IS_POWER_OF_2
#define IS_POWER_OF_2(x)	(((x)&(x-1))==0)
#endif

/* Enum ----------------------------------------------------------------------*/
/* Marco ---------------------------------------------------------------------*/
/* Typedef -------------------------------------------------------------------*/
/* Public Variable -----------------------------------------------------------*/
/* Public Class --------------------------------------------------------------*/
/* Public Function -----------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif

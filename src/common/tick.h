/**
 ******************************************************************************
 * @file	: tick.h
 * @version	: V0.0.1
 * @date	: 2023.09.01
 * @author	: Tony Liu
 ******************************************************************************
 *
 *
 *
 ******************************************************************************
 */

 /* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _TICK_H_
#define _TICK_H_

/* Predefined Or Compiler Option ---------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
/* Hardware Configuration ----------------------------------------------------*/
/* Define --------------------------------------------------------------------*/
#define ms(x) (x)
#define sec(x) (x * 1000)
#define min(x) (x * 1000 * 60)
#define hour(x) (x * 1000 * 60 * 60)

/* Enum ----------------------------------------------------------------------*/
/* Marco ---------------------------------------------------------------------*/
#define delay(time_unit) \
{\
    /* platform code */ \
    k_msleep(time_unit); \
}

#define delay_microsecond(time_unit) \
{\
    /* platform code */ \
    k_usleep(time_unit); \
}

/* platform code */ \
#define millis k_uptime_get

/* Typedef -------------------------------------------------------------------*/
typedef int64_t tick_t;

/* Public Variable -----------------------------------------------------------*/
/* Public Class --------------------------------------------------------------*/
/* Public Function -----------------------------------------------------------*/

/**
 * @brief Sets the timeout value
 * 
 * Sets the timeout value to the current uptime plus the specified 
 * number of milliseconds. This function can be used to implement 
 * non-blocking delays in a program.
 *
 * @param timeout Pointer to the timeout value to be set.
 * @param ms The number of milliseconds to add to the current uptime.
 */
extern void set_tick(tick_t *timeout, uint32_t ms);

/**
 * @brief Checks if the specified timeout has elapsed
 * 
 * Compares the specified timeout value with the current uptime 
 * to determine if the timeout has elapsed. This function can be 
 * used to implement non-blocking delays in a program.
 *
 * @param timeout The timeout value to compare with the current uptime.
 * @return true if the current uptime is greater than or equal to the timeout.
 * @return false otherwise.
 */
extern bool tick_is_timeout(tick_t timeout);

#ifdef __cplusplus
}
#endif

#endif

/** 
 ******************************************************************************
 * @file	: tick.c
 * @version	: V0.0.1
 * @date	: 2023.09.01
 * @author	: Tony Liu
 ******************************************************************************
 *
 *
 *
 ******************************************************************************
 */

/* Predefined Or Compiler Option ---------------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "common.h"

/* Private Typedef -----------------------------------------------------------*/
/* Private Prototype ---------------------------------------------------------*/
/* Private Define ------------------------------------------------------------*/
/* Private Enum --------------------------------------------------------------*/
/* Private Marco -------------------------------------------------------------*/
/* Private Variable ----------------------------------------------------------*/
/* Public Variable -----------------------------------------------------------*/
/* Import Variable -----------------------------------------------------------*/
/* Import Function -----------------------------------------------------------*/
/* Private Class -------------------------------------------------------------*/
/* Public Class --------------------------------------------------------------*/
/* Private Function ----------------------------------------------------------*/
/**
  * @brief	
  * @note	
  * @param	
  * @retval	
  * @return	
  */ 
  
/* Public Function -----------------------------------------------------------*/
/**
  * @brief	
  * @note	
  * @param	
  * @retval	
  * @return	
  */ 

/**
 * @brief Sets the timeout value
 * 
 * Sets the timeout value to the current uptime plus the specified 
 * number of milliseconds. This function can be used to implement 
 * non-blocking delays in a program.
 *
 * @param timeout Pointer to the timeout value to be set.
 * @param ms The number of milliseconds to add to the current uptime.
 */
void set_tick(tick_t *timeout, uint32_t ms)
{
  DEBUG_ASSERT(timeout != NULL);
  /* platform code */
  *timeout = k_uptime_get() + ms;
}

/**
 * @brief Checks if the specified timeout has elapsed
 * 
 * Compares the specified timeout value with the current uptime 
 * to determine if the timeout has elapsed. This function can be 
 * used to implement non-blocking delays in a program.
 *
 * @param timeout The timeout value to compare with the current uptime.
 * @return true if the current uptime is greater than or equal to the timeout.
 * @return false otherwise.
 */
bool tick_is_timeout(tick_t timeout)
{
  /* platform code */
  return k_uptime_delta(&timeout) >= 0;
}

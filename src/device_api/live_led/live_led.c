/** 
 ******************************************************************************
 * @file	: live_led.c
 * @version	: V0.0.1
 * @date	: 2023.09.02
 * @author	: Tony Liu
 ******************************************************************************
 *
 *
 *
 ******************************************************************************
 */

/* Predefined Or Compiler Option ---------------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include <zephyr/drivers/gpio.h>
#include "gpio_api.h"
#include "live_led.h"

/* Private Typedef -----------------------------------------------------------*/
/* Private Prototype ---------------------------------------------------------*/
/* Private Define ------------------------------------------------------------*/
#define LED0_NODE DT_ALIAS(led0)

/* Private Enum --------------------------------------------------------------*/
/* Private Marco -------------------------------------------------------------*/
/* Private Variable ----------------------------------------------------------*/
static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(LED0_NODE, gpios);

/* Public Variable -----------------------------------------------------------*/
/* Import Variable -----------------------------------------------------------*/
/* Import Function -----------------------------------------------------------*/
/* Private Class -------------------------------------------------------------*/
/* Public Class --------------------------------------------------------------*/
/* Private Function ----------------------------------------------------------*/
/**
  * @brief	
  * @note	
  * @param	
  * @retval	
  * @return	
  */ 
  
/* Public Function -----------------------------------------------------------*/
/**
  * @brief	
  * @note	
  * @param	
  * @retval	
  * @return	
  */ 

void init_live_led(void)
{
  configure_gpio_as_output(&led);
}

void update_and_control_live_led(void)
{
  static tick_t timeout = 0;

  if(tick_is_timeout(timeout))
  {
    set_tick(&timeout, LIVE_LED_TOGGLE_CYCLE);
    toggle_gpio(&led);
  }
}
